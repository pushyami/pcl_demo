#include "Pcl_IO.h"
using namespace pcl;
using namespace std;
using namespace boost;

Pcl_IO::Pcl_IO()
{
}


Pcl_IO::~Pcl_IO()
{
}

int Pcl_IO::write_pcd_file(PointCloud<PointXYZ>::Ptr cloud, string filePath)
{

	 pcl::io::savePCDFileASCII(filePath, *cloud);
	 
	 cerr << "Saved " << cloud->points.size() << " data points to test_pcd.pcd." << endl;

	 for (size_t i = 0; i < cloud->points.size(); ++i)
		 cerr << "    " << cloud->points[i].x << " " << cloud->points[i].y << " " << cloud->points[i].z << endl;

	 return (0);
}

PointCloud<PointXYZ>::Ptr Pcl_IO::read_pcd_file(string filePath){

	PointCloud<PointXYZ>::Ptr cloud(new PointCloud<PointXYZ>);
	
	if (pcl::io::loadPCDFile<PointXYZ>(filePath, *cloud) == -1) //* load the file
	{
		PCL_ERROR("Couldn't read file test_pcd.pcd \n");
	}
	cout << "Loaded "
		<< cloud->width * cloud->height
		<< " data points from test_pcd.pcd with the following fields: "
		<< endl;

	for (size_t i = 0; i < cloud->points.size(); ++i){
		cloud->points[i].x = cloud->points[i].x / 100;
		cloud->points[i].y = cloud->points[i].y / 100;
		cloud->points[i].z = cloud->points[i].z / 100;
	}
	/*for (size_t i = 0; i < cloud.points.size (); ++i)
	std::cout << "    " << cloud.points[i].x
	<< " "    << cloud.points[i].y
	<< " "    << cloud.points[i].z << std::endl;*/

	return cloud;

}

int Pcl_IO::write_csv_file(PointCloud<PointXYZ>::Ptr pcloud_ptr, string filePath){

	ofstream csv_file;
	csv_file.open(filePath);
	for (size_t i = 0; i < pcloud_ptr->points.size(); ++i){
		csv_file << pcloud_ptr->points[i].x << "," << pcloud_ptr->points[i].y << "," << pcloud_ptr->points[i].z << endl;
	}

	csv_file.close();
	return 0;
}

int Pcl_IO::write_csv_file(PointCloud<PointXYZRGB>::Ptr pcloud_ptr, string filePath){

	ofstream csv_file;
	csv_file.open(filePath);
	for (size_t i = 0; i < pcloud_ptr->points.size(); ++i){
		csv_file << pcloud_ptr->points[i].x << "," << pcloud_ptr->points[i].y << "," << pcloud_ptr->points[i].z <<","<< (pcloud_ptr->points[i].r/5)<< endl;
	}

	csv_file.close();
	return 0;
}

void Pcl_IO::read_csv_file(string filePath, PointCloud<PointXYZ>::Ptr cloud){

	ifstream csv_file;
	string line;
	vector<string> fields;
	csv_file.open(filePath);
	
	if (csv_file.is_open())
	{
		int count = 0;
		while (getline(csv_file, line))
		{
			boost::split(fields, line, is_any_of(","));
			cloud->push_back(PointXYZ(stof(fields[0]) / 10, stof(fields[1]) / 10, stof(fields[2]) / 10));
			count++;
		}

		csv_file.close();
		
	}
	else{
		cout << "Unable to open file";

	}
	cloud->is_dense = false;
}

void Pcl_IO::read_csv_file(string filePath, PointCloud<PointXYZRGB>::Ptr cloudrgb){

	ifstream csv_file;
	string line;
	vector<string> fields;
	csv_file.open(filePath);
	if (csv_file.is_open())
	{
		int count = 0;
		while (getline(csv_file, line))
		{
			boost::split(fields, line, is_any_of(","));
			PointXYZRGB p = PointXYZRGB();
			p.x = stof(fields[0]);
			p.y = stof(fields[1]);
			p.z = stof(fields[2]);

			p.r = stof(fields[3])*5;
			p.g = stof(fields[3])*5;
			p.b = stof(fields[3])*5;
			cloudrgb->push_back(p);
			
			count++;
		}

		csv_file.close();

	}
	else{
		cout << "Unable to open file";

	}
	cloudrgb->is_dense = false;
	
}

////////////////////////
//Max's OBJ parsing code
////////////////////////

vector<string> &Pcl_IO::split(const string &s, char delim, vector<string> &elems) {
	stringstream ss(s);
	string item;
	while (getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return elems;
}

vector<string> Pcl_IO::split(const string &s, char delim) {
	vector<string> elems;
	split(s, delim, elems);
	return elems;
}

PointCloud<PointXYZRGB>::Ptr Pcl_IO::ingestPCFile(string fname, PointCloud<PointXYZRGB>::Ptr cloud, int r, int g, int b){
    cout << fname << endl;

	string line;
	ifstream pcFile(fname);
	int i = 0;

	if (pcFile.is_open()){
		while (getline(pcFile, line)){
			//cout << line << endl;			
			if (line.substr(0, 2) == "v "){
				i++;
			}
		}
	}
	pcFile.close();
	pcFile.open(fname);
	cout << "There were " << i << " vertices." << endl;

	cloud->width = (int)(sqrt(i) + 1);
	cloud->height = (int)(sqrt(i) + 1);
	cloud->is_dense = true;
	cloud->points.resize(cloud->width * cloud->height);

	vector<string> lineVec;
	string::size_type sz;
	i = 0;
	if (pcFile.is_open()){
		while (getline(pcFile, line)){
			//cout << line << endl;			
			if (line.substr(0, 2) == "v "){
				lineVec = split(line, ' ');

				/*
				if (i % 1000 == 0){
				cout << std::stof(lineVec[1], &sz) << ", " << std::stof(lineVec[2], &sz) << ", " << std::stof(lineVec[3], &sz) << endl;
				}
				//*/
				cloud->points[i].x = stof(lineVec[1], &sz)/100;
				cloud->points[i].y = stof(lineVec[2], &sz)/100;
				cloud->points[i].z = stof(lineVec[3], &sz)/100;
				cloud->points[i].r = stof(lineVec[4], &sz)*512;
				cloud->points[i].g = stof(lineVec[5], &sz)*512;
				cloud->points[i].b = stof(lineVec[6], &sz)*512;
				i++;

			}
		}
	}


	return cloud;
}
