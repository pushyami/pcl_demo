#pragma once

#define _CRT_SECURE_NO_WARNINGS

#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/passthrough.h>
#include <iostream>
#include <pcl/point_types.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/features/normal_3d.h>
#include <boost/algorithm/string.hpp>
#include <pcl/PCLPointCloud2.h>
#include <pcl/surface/gp3.h>
#include <pcl/io/vtk_io.h>
#include <pcl/io/obj_io.h>

class Pcl_Processing
{
public:
	Pcl_Processing();
	~Pcl_Processing();
	pcl::PointCloud<pcl::Normal>::Ptr computeNormals(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, double radius);
	int passThroughFilter(const pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudIn, pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudOut, std::string axis, float min, float max);
	int radialOutlierFilter(const pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudIn, pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudOut, float radius, float neighbours);
	int voxelGridFilter(const pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudIn, pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudOut, float x, float y, float z);
	pcl::PolygonMesh greedyTriangulation(pcl::PointCloud<pcl::PointXYZ>::Ptr cloudIn);
	void findKeyPoints(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, float min_scale, int no_octaves, int no_scales_perOctave, float min_contrast, pcl::PointCloud<pcl::PointWithScale>::Ptr keyPoints);
	void extractSHOTCOLORFeatureDescriptors(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudRGB, pcl::PointCloud<pcl::PointWithScale>::Ptr keyPoints, pcl::PointCloud<pcl::SHOT1344>::Ptr descriptors);
	void Pcl_Processing::extractFPFHFeatureDescriptors(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, pcl::PointCloud<pcl::PointWithScale>::Ptr keyPoints, pcl::PointCloud<pcl::FPFHSignature33>::Ptr descriptors);
	void Pcl_Processing::extractPFHRGBFeatureDescriptors(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, pcl::PointCloud<pcl::PointWithScale>::Ptr keyPoints, pcl::PointCloud<pcl::PFHRGBSignature250>::Ptr descriptors);
	void findCorrespondences(pcl::PointCloud<pcl::PFHRGBSignature250>::Ptr sourceDescriptors, pcl::PointCloud<pcl::PFHRGBSignature250>::Ptr targetDescriptors, std::vector<int> &correspondences_out, std::vector<float> &correspondence_scores_out);
	void findCorrespondences(pcl::PointCloud<pcl::FPFHSignature33>::Ptr sourceDescriptors, pcl::PointCloud<pcl::FPFHSignature33>::Ptr targetDescriptors, pcl::Correspondences &all_correspondences);
	void findCorrespondences(pcl::PointCloud<pcl::PFHRGBSignature250>::Ptr sourceDescriptors, pcl::PointCloud<pcl::PFHRGBSignature250>::Ptr targetDescriptors, pcl::Correspondences &all_correspondences);
	pcl::CorrespondencesPtr filterCorrespondences(std::vector<int> &correspondences1, std::vector<int> &correspondences2, pcl::PointCloud<pcl::PointWithScale>::Ptr keyPointsSrc, pcl::PointCloud<pcl::PointWithScale>::Ptr keyPointsDst, float medianFactor);
	pcl::CorrespondencesPtr filterCorrespondences(const pcl::CorrespondencesPtr &all_correspondences, const pcl::PointCloud<pcl::PointWithScale>::Ptr &keypoints_src, const pcl::PointCloud<pcl::PointWithScale>::Ptr &keypoints_tgt, float threshold);
	Eigen::Matrix4f findTransformation(pcl::PointCloud<pcl::PointWithScale>::Ptr keyPointsSrc, pcl::PointCloud<pcl::PointWithScale>::Ptr keyPointsDst, pcl::CorrespondencesPtr corrs);


};

