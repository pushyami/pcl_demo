#include "Pcl_Processing.h"

#include <pcl/keypoints/keypoint.h>
#include <pcl/keypoints/sift_keypoint.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/correspondence_estimation.h>
#include <pcl/registration/correspondence_rejection_distance.h>
#include <pcl/registration/correspondence_rejection_sample_consensus.h>
#include <pcl/registration/correspondence_rejection_median_distance.h>
#include <pcl/features/shot.h>
#include <pcl/features/fpfh.h>
#include <pcl/features/pfhrgb.h>

using namespace pcl;
using namespace boost;
using namespace std;

Pcl_Processing::Pcl_Processing()
{
}


Pcl_Processing::~Pcl_Processing()
{
}

PointCloud<Normal>::Ptr Pcl_Processing::computeNormals(PointCloud<PointXYZ>::Ptr cloud, double radius){

	// ----------------------------------------------------------------
	// -----Calculate surface normals with a search radius of 0.05-----
	// ----------------------------------------------------------------

	NormalEstimation<PointXYZ, Normal> ne;
	pcl::search::KdTree<PointXYZ>::Ptr tree(new pcl::search::KdTree<PointXYZ>());
	ne.setInputCloud(cloud);
	ne.setSearchMethod(tree);
	PointCloud<Normal>::Ptr cloud_normals1(new PointCloud<Normal>);
	ne.setRadiusSearch(radius);
	ne.compute(*cloud_normals1);
	return cloud_normals1;
}

///-----------------------------------///
/// implementation of various filters///
///----------------------------------///

//Pass through Filtering//
int Pcl_Processing::passThroughFilter(const PointCloud<PointXYZRGB>::Ptr cloudIn, PointCloud<PointXYZRGB>::Ptr cloudOut, string axis, float min, float max){

	PointCloud<PointXYZRGB>::Ptr cloud_filtered(new PointCloud<PointXYZRGB>);
		
	cout << "number of points before filtering" << cloudIn->points.size() << endl;

	//pointcloud filtering using passthrough filter
	
	PassThrough<PointXYZRGB> filter;
	filter.setInputCloud(cloudIn);
	filter.setFilterFieldName(axis);
	filter.setFilterLimits(min, max);  //-7.1, -5.6
	filter.filter(*cloud_filtered);

	*cloudOut = *cloud_filtered;
	cout << "number of points after filtering" << cloudOut->points.size() << endl;
	
	return 0;
}

//Radial Outlier filtering//
int Pcl_Processing::radialOutlierFilter(const PointCloud<PointXYZRGB>::Ptr cloudIn, PointCloud<PointXYZRGB>::Ptr cloudOut, float radius, float neighbours){

	PointCloud<PointXYZRGB>::Ptr cloud_filtered(new PointCloud<PointXYZRGB>);
	

	//pointcloud filtering using radial outlier removal
	RadiusOutlierRemoval< PointXYZRGB> rad_outlier_filter;
	rad_outlier_filter.setInputCloud(cloudIn);
	rad_outlier_filter.setRadiusSearch(radius); //0.01
	rad_outlier_filter.setMinNeighborsInRadius(neighbours); // 4
	rad_outlier_filter.filter(*cloud_filtered);

	*cloudOut = *cloud_filtered;

	
	cout << "number of points after filtering" << cloudOut->points.size() << endl;

	return 0;
}

// voxel grid filtering//
int Pcl_Processing::voxelGridFilter(const PointCloud<PointXYZRGB>::Ptr cloudIn, PointCloud<PointXYZRGB>::Ptr cloudOut, float x, float y, float z){

	PointCloud<PointXYZRGB>::Ptr cloud_filtered(new PointCloud<PointXYZRGB>);
	PCLPointCloud2::Ptr cloudIn2(new PCLPointCloud2());
	PCLPointCloud2::Ptr cloud_filtered2(new PCLPointCloud2());
	
	cout << "number of points before filtering" << cloudIn->points.size() << endl;

	//convert the point cloud into PointCloud2
	toPCLPointCloud2(*cloudIn, *cloudIn2);

	// pass the pointcloud into a voxelgrid filter
	VoxelGrid<PCLPointCloud2> filter;
	filter.setInputCloud(cloudIn2);
	filter.setLeafSize(x, y, z); //0.025, 0.025, 0.025
	filter.filter(*cloud_filtered2);

	//convert from pointcloud2 to pointcloud
	fromPCLPointCloud2(*cloud_filtered2, *cloud_filtered);

	*cloudOut = *cloud_filtered;

	cout << "number of points after filtering" << cloudOut->points.size() << endl;
	
	return 0;
}

///------------------------------///
/// Key Point Extraction Methods///
///-----------------------------///

//Method which find SIFT key points
void Pcl_Processing::findKeyPoints(PointCloud<PointXYZRGB>::Ptr cloud, float min_scale, int no_octaves, int no_scales_perOctave, float min_contrast, PointCloud<PointWithScale>::Ptr keyPoints)
{

	SIFTKeypoint<PointXYZRGB, PointWithScale> sift;


	pcl::search::KdTree<PointXYZRGB>::Ptr tree(new pcl::search::KdTree<PointXYZRGB>());
	sift.setSearchMethod(tree);
	sift.setKSearch(100);
	sift.setScales(min_scale, no_octaves, no_scales_perOctave);
	sift.setMinimumContrast(min_contrast);
	sift.setInputCloud(cloud);
	sift.compute(*keyPoints);

}



///----------------------------------------///
/// Feature extraction methods            ///
///---------------------------------------///

// Implementation for extracting Fast point feature historgram descriptors//
void Pcl_Processing::extractFPFHFeatureDescriptors(PointCloud<PointXYZRGB>::Ptr cloud, PointCloud<PointWithScale>::Ptr keyPoints, PointCloud<FPFHSignature33>::Ptr descriptors)
{
	//caluclate normal
	Pcl_Processing processor = Pcl_Processing();
	PointCloud<PointXYZ>::Ptr cloudXYZ(new PointCloud<PointXYZ>());
	copyPointCloud(*cloud, *cloudXYZ);

	PointCloud<Normal>::Ptr normals = processor.computeNormals(cloudXYZ, 0.04);


	//feature extractin FPFH
	FPFHEstimation<PointXYZ, Normal, FPFHSignature33> fpfh_est;
	pcl::search::KdTree<PointXYZ>::Ptr  tree(new pcl::search::KdTree<PointXYZ>());
	fpfh_est.setSearchMethod(tree);
	fpfh_est.setRadiusSearch(0.07);

	

	PointCloud<PointXYZ>::Ptr keyPointsXYZ(new PointCloud<PointXYZ>());
	copyPointCloud(*keyPoints, *keyPointsXYZ);

	fpfh_est.setSearchSurface(cloudXYZ);
	fpfh_est.setInputNormals(normals);
	fpfh_est.setInputCloud(keyPointsXYZ);

	fpfh_est.compute(*descriptors);
}

// Implementation for extracting Signature Historgram of orientations with color descriptors//
void Pcl_Processing::extractSHOTCOLORFeatureDescriptors(PointCloud<PointXYZRGB>::Ptr cloudRGB, PointCloud<PointWithScale>::Ptr keyPoints, PointCloud<SHOT1344>::Ptr descriptors)
{
	//caluclate normal
	Pcl_Processing processor = Pcl_Processing();
	PointCloud<PointXYZ>::Ptr tempCloud(new PointCloud<PointXYZ>());
	copyPointCloud(*cloudRGB, *tempCloud);
	PointCloud<Normal>::Ptr normals = processor.computeNormals(tempCloud, 0.04);


	//feature extractin SHOT
	SHOTColorEstimation<PointXYZRGB, Normal, SHOT1344> shot_est;
	pcl::search::KdTree<PointXYZRGB>::Ptr  tree(new pcl::search::KdTree<PointXYZRGB>());
	shot_est.setSearchMethod(tree);
	shot_est.setRadiusSearch(0.07);
	PointCloud<PointXYZRGB>::Ptr keyPointsRGB(new PointCloud<PointXYZRGB>());
	copyPointCloud(*keyPoints, *keyPointsRGB);

	shot_est.setSearchSurface(cloudRGB);
	shot_est.setInputNormals(normals);
	shot_est.setInputCloud(keyPointsRGB);

	shot_est.compute(*descriptors);
}



//Implementation for extracting point feature histograms with RGB data descriptors
void Pcl_Processing::extractPFHRGBFeatureDescriptors(PointCloud<PointXYZRGB>::Ptr cloud, PointCloud<PointWithScale>::Ptr keyPoints, PointCloud<PFHRGBSignature250>::Ptr descriptors)
{
	//caluclate normal
	Pcl_Processing processor = Pcl_Processing();
	PointCloud<PointXYZ>::Ptr cloudXYZ(new PointCloud<PointXYZ>());
	copyPointCloud(*cloud, *cloudXYZ);

	PointCloud<Normal>::Ptr normals = processor.computeNormals(cloudXYZ, 0.04);


	//feature extractin FPFH
	PFHRGBEstimation<PointXYZRGB, Normal, PFHRGBSignature250> pfhrgb_est;
	pcl::search::KdTree<PointXYZRGB>::Ptr  tree(new pcl::search::KdTree<PointXYZRGB>());
	pfhrgb_est.setSearchMethod(tree);
	pfhrgb_est.setRadiusSearch(0.07);



	PointCloud<PointXYZRGB>::Ptr keyPointsRGB(new PointCloud<PointXYZRGB>());
	copyPointCloud(*keyPoints, *keyPointsRGB);

	pfhrgb_est.setSearchSurface(cloud);
	pfhrgb_est.setInputNormals(normals);
	pfhrgb_est.setInputCloud(keyPointsRGB);

	pfhrgb_est.compute(*descriptors);
}


///------------------------------------------------///
/// Determining correspondences and filtering them ///
///------------------------------------------------///

//Implementation for fidning corespondences using correspondences estimator//
void Pcl_Processing::findCorrespondences(pcl::PointCloud<pcl::PFHRGBSignature250>::Ptr sourceDescriptors, pcl::PointCloud<pcl::PFHRGBSignature250>::Ptr targetDescriptors, pcl::Correspondences &all_correspondences)
{
	pcl::registration::CorrespondenceEstimation <PFHRGBSignature250, PFHRGBSignature250> est;
	est.setInputSource(sourceDescriptors);
	est.setInputTarget(targetDescriptors);
	est.determineReciprocalCorrespondences(all_correspondences);
}

void Pcl_Processing::findCorrespondences(pcl::PointCloud<pcl::FPFHSignature33>::Ptr sourceDescriptors, pcl::PointCloud<pcl::FPFHSignature33>::Ptr targetDescriptors, pcl::Correspondences &all_correspondences)
{
	pcl::registration::CorrespondenceEstimation <FPFHSignature33, FPFHSignature33> est;
	est.setInputSource(sourceDescriptors);
	est.setInputTarget(targetDescriptors);
	est.determineReciprocalCorrespondences(all_correspondences);
}

//implementation for finding correspondences from scratch and it seems to work fine
void Pcl_Processing::findCorrespondences(PointCloud<PFHRGBSignature250>::Ptr sourceDescriptors, PointCloud<PFHRGBSignature250>::Ptr targetDescriptors, vector<int> &correspondences_out, vector<float> &correspondence_scores_out)
{

	correspondences_out.resize(sourceDescriptors->size());
	correspondence_scores_out.resize(sourceDescriptors->size());

	KdTreeFLANN<PFHRGBSignature250> tree;
	tree.setInputCloud(targetDescriptors);

	const int k = 1;
	vector<int> k_indices(k);
	vector<float> k_squared_distances(k);
	for (size_t i = 0; i < sourceDescriptors->size(); ++i)
	{
		
		/*if (pcl_isnan(sourceDescriptors->at(i).descriptor[0]))
			continue;*/
		tree.nearestKSearch(*sourceDescriptors, i, k, k_indices, k_squared_distances);
		correspondences_out[i] = k_indices[0];
		correspondence_scores_out[i] = k_squared_distances[0];
	}

}

//filtering out bad correspondences
CorrespondencesPtr Pcl_Processing::filterCorrespondences(const CorrespondencesPtr &all_correspondences,	const PointCloud<PointWithScale>::Ptr &keypoints_src,	const PointCloud<PointWithScale>::Ptr &keypoints_tgt, float threshold)
	
{
	PointCloud<PointXYZ>::Ptr keyPointsXYZsrc(new PointCloud<PointXYZ>());
	PointCloud<PointXYZ>::Ptr keyPointsXYZdst(new PointCloud<PointXYZ>());
	copyPointCloud(*keypoints_src, *keyPointsXYZsrc);
	copyPointCloud(*keypoints_tgt, *keyPointsXYZdst);

	/*pcl::registration::CorrespondenceRejectorMedianDistance rejector;


	rejector.setInputSource<PointXYZ>(keyPointsXYZsrc);
	rejector.setInputTarget<PointXYZ>(keyPointsXYZdst);
	rejector.setInputCorrespondences(all_correspondences);
	rejector.setMedianFactor(1.0);
	rejector.getCorrespondences(*all_correspondences);
	cout << "median distance" << rejector.getMedianDistance() << endl;*/


	pcl::registration::CorrespondenceRejectorSampleConsensus<PointXYZ> rejector;
	CorrespondencesPtr remaining(new Correspondences);
	rejector.setInputSource(keyPointsXYZsrc);
	rejector.setInputTarget(keyPointsXYZdst);
	rejector.setInlierThreshold(threshold);
	rejector.setInputCorrespondences(all_correspondences);
	rejector.getCorrespondences(*remaining);

	//pcl::registration::CorrespondenceRejectorDistance rej;
	//rej.setInputCloud<PointXYZ>(keyPointsXYZsrc);
	//rej.setInputTarget<PointXYZ>(keyPointsXYZdst);
	//rej.setMaximumDistance(threshold);    // 1m
	//rej.setInputCorrespondences(all_correspondences);
	//rej.getCorrespondences(*all_correspondences);
	return remaining;

}
CorrespondencesPtr Pcl_Processing::filterCorrespondences(vector<int> &correspondences1, vector<int> &correspondences2, PointCloud<PointWithScale>::Ptr keyPointsSrc, PointCloud<PointWithScale>::Ptr keyPointsDst, float threshold)
{
	std::vector<std::pair<unsigned, unsigned> > correspondences;
	CorrespondencesPtr correspondencePtr(new Correspondences);
	for (unsigned i = 0; i < correspondences1.size(); ++i){
		if (correspondences2[correspondences1[i]] == i){

			correspondences.push_back(std::make_pair(i, correspondences1[i]));
		}
	}

	correspondencePtr->resize(correspondences.size());
	for (unsigned i = 0; i < correspondences.size(); ++i){
		(*correspondencePtr)[i].index_query = correspondences[i].first;
		(*correspondencePtr)[i].index_match = correspondences[i].second;
	}

	
	pcl::registration::CorrespondenceRejectorSampleConsensus<PointWithScale> rejector;
	CorrespondencesPtr remaining(new Correspondences);
	rejector.setInputSource(keyPointsSrc);
	rejector.setInputTarget(keyPointsDst);
	rejector.setInlierThreshold(threshold);
	rejector.setInputCorrespondences(correspondencePtr);
	rejector.getCorrespondences(*remaining);

	/*pcl::registration::CorrespondenceRejectorMedianDistance rejector;
	

	rejector.setInputSource<PointWithScale>(keyPointsSrc);
	rejector.setInputTarget<PointWithScale>(keyPointsDst);
	rejector.setInputCorrespondences(correspondencePtr);
	rejector.setMedianFactor(threshold);
	rejector.getCorrespondences(*remaining);
	cout <<"median distance"<< rejector.getMedianDistance() << endl;*/


	return remaining;
}


Eigen::Matrix4f Pcl_Processing::findTransformation(PointCloud<PointWithScale>::Ptr keyPointsSrc, PointCloud<PointWithScale>::Ptr keyPointsDst, CorrespondencesPtr corrs)
{
	Eigen::Matrix4f transformationSVD;
	pcl::registration::TransformationEstimationSVD<PointWithScale, PointWithScale> svd;

	svd.estimateRigidTransformation(*keyPointsSrc, *keyPointsDst, *corrs, transformationSVD);
	return transformationSVD;
}

PolygonMesh Pcl_Processing::greedyTriangulation(PointCloud<PointXYZ>::Ptr cloudIn){

	// Normal estimation*
	NormalEstimation<PointXYZ, Normal> n;
	PointCloud<Normal>::Ptr normals(new PointCloud<Normal>);
	pcl::search::KdTree<PointXYZ>::Ptr tree(new pcl::search::KdTree<PointXYZ>);
	tree->setInputCloud(cloudIn);
	n.setInputCloud(cloudIn);
	n.setSearchMethod(tree);
	n.setKSearch(10);
	n.compute(*normals);

	//* normals should not contain the point normals + surface curvatures
	// Concatenate the XYZ and normal fields*
	PointCloud<PointNormal>::Ptr cloud_with_normals(new PointCloud<PointNormal>);
	concatenateFields(*cloudIn, *normals, *cloud_with_normals);
	//* cloud_with_normals = cloud + normals

	// Create search tree*
	pcl::search::KdTree<PointNormal>::Ptr tree2(new pcl::search::KdTree<PointNormal>);
	tree2->setInputCloud(cloud_with_normals);
	// Initialize objects
	pcl::GreedyProjectionTriangulation<PointNormal> gp3;
	pcl::PolygonMesh triangles;
	// Set the maximum distance between connected points (maximum edge length)
	gp3.setSearchRadius(0.045);
	// Set typical values for the parameters
	gp3.setMu(1.5);
	gp3.setMaximumNearestNeighbors(100);
	gp3.setMaximumSurfaceAngle(M_PI / 2); // 180 degrees
	gp3.setMinimumAngle(M_PI / 36); // 5 degrees
	gp3.setMaximumAngle(2 * M_PI); // 180 degrees
	gp3.setNormalConsistency(false);

	// Get result
	gp3.setInputCloud(cloud_with_normals);
	gp3.setSearchMethod(tree2);
	gp3.reconstruct(triangles);

	// Additional vertex information
	vector<int> parts = gp3.getPartIDs();
	vector<int> states = gp3.getPointStates();

	pcl::io::saveVTKFile("mesh.vtk", triangles);
	pcl::io::saveOBJFile("pointcloud_obj.obj", triangles);
	// Finish
	cout << triangles.polygons.size() << endl;
	return triangles;
}