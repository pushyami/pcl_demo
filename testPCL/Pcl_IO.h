#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <boost/algorithm/string.hpp>


class Pcl_IO
{

public:
	Pcl_IO();
	~Pcl_IO();
	int write_pcd_file(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, std::string filePath);
	pcl::PointCloud<pcl::PointXYZ>::Ptr read_pcd_file(std::string filePath);
	int write_csv_file(pcl::PointCloud<pcl::PointXYZ>::Ptr pcloud_ptr, std::string filePath);
	int write_csv_file(pcl::PointCloud<pcl::PointXYZRGB>::Ptr pcloud_ptr, std::string filePath);
	void  read_csv_file(std::string filePath, pcl::PointCloud<pcl::PointXYZ>::Ptr cloud);
	void  read_csv_file(std::string filePath, pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud);
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr ingestPCFile(std::string fname, pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, int r, int g, int b);

private:
	std::vector<std::string> split(const std::string &s, char delim);
	std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);

};

