#define _CRT_SECURE_NO_WARNINGS

#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/visualization/cloud_viewer.h>
#include <iostream>
#include <pcl/keypoints/keypoint.h>
#include <pcl/keypoints/sift_keypoint.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/correspondence_rejection_sample_consensus.h>
#include <pcl/features/shot.h>
#include "Pcl_IO.h"
#include "Pcl_Visualization.h"
#include "Pcl_Processing.h"

using namespace std;
using namespace boost;
using namespace pcl;

PointCloud<pcl::PointXYZRGB>::Ptr cloud (new PointCloud<PointXYZRGB>);

int gen_random_points()
{
	// Fill in the cloud data
	cloud->width = 100;
	cloud->height = 100;
	cloud->is_dense = false;
	cloud->points.resize(cloud->width * cloud->height);

	for (size_t i = 0; i < cloud->points.size(); ++i)
	{
		cloud->points[i].x = rand() % 10;
		cloud->points[i].y = rand() % 10; 
		cloud->points[i].z = rand() % 10;
		cloud->points[i].r = rand() % 255; 
		cloud->points[i].g = rand() % 255; 
		cloud->points[i].b = rand() % 255; 
	}

	return 0;
}


int main(int argc, char *argv[])
{
		

	Pcl_IO io = Pcl_IO();
	Pcl_Processing p = Pcl_Processing();


	PointCloud<PointXYZRGB>::Ptr cloudRGB(new PointCloud<PointXYZRGB>());
	PointCloud<PointXYZRGB>::Ptr cloudRGB2(new PointCloud<PointXYZRGB>());
	PointCloud<PointXYZRGB>::Ptr cloudFiltered(new PointCloud<PointXYZRGB>());
	
	io.read_csv_file("../pclouds/stitching_dataSet/pc_filtered_1.csv", cloudRGB);
	io.read_csv_file("../pclouds/stitching_dataSet/pc_filtered_4.csv", cloudRGB2);
	
	
	Eigen::Matrix4f transform = Eigen::Matrix4f::Identity();
	transform(2, 3) = 2.0;
	transformPointCloud(*cloudRGB2, *cloudRGB2, transform);

	float min_scale, no_octaves, scalesPerOctave, min_cont, threshold;
	cout << "enter the min scale for SIFT:" << endl;
	cin >> min_scale;
	cout << "enter the no of octaves for SIFT:" << endl;
	cin >> no_octaves;
	cout << "enter the no of scales per octave for SIFT:" << endl;
	cin >> scalesPerOctave;
	cout << "enter the min contrast for SIFT:" << endl;
	cin >> min_cont;
	cout << "enter the threshold for correspondences rejection:" << endl;
	cin >> threshold;
	// Find Keypoints, currently detecting SIFT.

	PointCloud<PointWithScale>::Ptr keyPoints(new PointCloud<PointWithScale>());
	p.findKeyPoints(cloudRGB, min_scale, no_octaves, scalesPerOctave, min_cont, keyPoints);
	cout << "no of key points found" << keyPoints->points.size() << endl;

	PointCloud<PointWithScale>::Ptr keyPoints2(new PointCloud<PointWithScale>());
	p.findKeyPoints(cloudRGB2, min_scale, no_octaves, scalesPerOctave, min_cont, keyPoints2);
	cout << "no of key points found" << keyPoints2->points.size() << endl;

	//Find Descriptors at each Key point. SHOTColor Descriptors
	
	//PointCloud<SHOT1344>::Ptr descriptorsSrc(new PointCloud<SHOT1344>());
	//p.extractSHOTCOLORFeatureDescriptors(cloudRGB, keyPoints, descriptorsSrc);
	//PointCloud<SHOT1344>::Ptr descriptorsTgt(new PointCloud<SHOT1344>());
	//p.extractSHOTCOLORFeatureDescriptors(cloudRGB2, keyPoints2, descriptorsTgt);
	//
	//std::vector<int> corr_src2Tgt;
	//std::vector<int> corr_Tgt2Src;
	//std::vector<float> correspondence_scores;

	////// Determine correspondences and filter bad correspondences
	//p.findCorrespondences(descriptorsSrc, descriptorsTgt, corr_src2Tgt, correspondence_scores);
	//p.findCorrespondences(descriptorsTgt, descriptorsSrc, corr_Tgt2Src, correspondence_scores);

 //   CorrespondencesPtr correspondences = p.filterCorrespondences(corr_src2Tgt, corr_Tgt2Src, keyPoints, keyPoints2, medianFactor);
	
	//determine FPFH descriptors at all keypoints
	//PointCloud<FPFHSignature33>::Ptr descriptorsSrc(new PointCloud<FPFHSignature33>());
	//p.extractFPFHFeatureDescriptors(cloudRGB, keyPoints, descriptorsSrc);
	
	//PointCloud<FPFHSignature33>::Ptr descriptorsTgt(new PointCloud<FPFHSignature33>());
	//p.extractFPFHFeatureDescriptors(cloudRGB2, keyPoints2, descriptorsTgt);

	//determine eciprocal correspondences
	//CorrespondencesPtr correspondences(new Correspondences);
	//p.findCorrespondences(descriptorsSrc, descriptorsTgt, *correspondences);
	//reject bad correspondences
	//p.filterCorrespondences(correspondences, keyPoints, keyPoints2, 1.0);

	//determine PFHRGB descriptors at all keypoints
	PointCloud<PFHRGBSignature250>::Ptr descriptorsSrc(new PointCloud<PFHRGBSignature250>());
	p.extractPFHRGBFeatureDescriptors(cloudRGB, keyPoints, descriptorsSrc);

	PointCloud<PFHRGBSignature250>::Ptr descriptorsTgt(new PointCloud<PFHRGBSignature250>());
	p.extractPFHRGBFeatureDescriptors(cloudRGB2, keyPoints2, descriptorsTgt);


	std::vector<int> corr_src2Tgt;
	std::vector<int> corr_Tgt2Src;
	std::vector<float> correspondence_scores;

	//// Determine correspondences and filter bad correspondences
	p.findCorrespondences(descriptorsSrc, descriptorsTgt, corr_src2Tgt, correspondence_scores);
	p.findCorrespondences(descriptorsTgt, descriptorsSrc, corr_Tgt2Src, correspondence_scores);

	CorrespondencesPtr correspondences = p.filterCorrespondences(corr_src2Tgt, corr_Tgt2Src, keyPoints, keyPoints2, threshold);

	// determine the transformatio between the point clouds
	Eigen::Matrix4f tranform2 = p.findTransformation(keyPoints, keyPoints2, correspondences);
	std::cout << "\ntransformation from SVD is \n" << tranform2;
	transformPointCloud(*cloudRGB, *cloudRGB, tranform2);

	//initialize the visualizer
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("3D Viewer"));
	viewer->setBackgroundColor(0, 0, 0);
	pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(cloudRGB);
	pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb2(cloudRGB2);
	viewer->addPointCloud<pcl::PointXYZRGB>(cloudRGB, rgb, "cloud1");
	viewer->addPointCloud<pcl::PointXYZRGB>(cloudRGB2, rgb2, "cloud2");
	

    viewer->addPointCloud<PointWithScale>(keyPoints, "key1");
	viewer->addPointCloud<PointWithScale>(keyPoints2, "key2");

	viewer->addCorrespondences<PointWithScale>(keyPoints, keyPoints2, *correspondences, "correspndences");
	viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 6, "key1");
    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 6, "key2");
	//viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "cloud1");
	//viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "cloud2");

	//viewer->addCoordinateSystem(1.0);
	viewer->initCameraParameters();
	
	while (!viewer->wasStopped())
	{
		viewer->spinOnce(10000);
		boost::this_thread::sleep(boost::posix_time::microseconds(100000));
	}
	
	
}
